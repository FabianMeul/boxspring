module.exports = function(grunt) {

    var assetsFolder = "theme/assets/",
        scssDir = assetsFolder + 'scss',
        cssDir = assetsFolder + 'css';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            options: {
                browsers: ['last 3 versions']
            },
            css: {
                expand: true,
                src: cssDir + '*.css',
                dest: cssDir
            },
        },

        sass: {
            theme: {
                options: {
                    bundleExec: true,
                    style: 'expanded',
                    require: ['sass-globbing'],
                    sourcemap: 'none'
                },
                files: [{
                    expand: true,
                    cwd: scssDir,
                    src: ['**/*.scss'],
                    dest: cssDir,
                    ext: '.css'
                }]
            }
        },

        watch: {
            styles: {
                files: ['**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            }
        }
    });

    // Load Grunt plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // Default task(s).
    grunt.registerTask('default', ['sass:theme', 'autoprefixer', 'watch']);

};

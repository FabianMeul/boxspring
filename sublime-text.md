# Sublime Text

At District01, our front-end developers often use [Sublime Text 3](http://www.sublimetext.com/).

**Table of contents**

* [Plugins](#plugins)



<a name="plugins"></a>
## Plugins
The easiest way to install plugins for Sublime Text is via [Package Control](https://sublime.wbond.net/installation). We highly recommend to install Package Control. :-)

#### Workflow
Our projects use Gulp or Grunt workflows to streamline recurring processes. The following Sublime Text plugins assist in running tasks straight from your editor.

* [Sublime Gulp](https://github.com/NicoSantangelo/sublime-gulp)
* [Sublime Grunt](https://github.com/tvooo/sublime-grunt)

### SASS
* [ColorHighlighter](https://github.com/Monnoroch/ColorHighlighter)
* [Syntax Highlighting for SASS](https://github.com/P233/Syntax-highlighting-for-Sass)

### Javascript
* [JsFormat](https://github.com/jdc0589/JsFormat)

### Git
* [Git](https://github.com/kemayo/sublime-text-git)
* [Sublime FileDiffs](https://github.com/colinta/SublimeFileDiffs)

### Markdown
* [Sublime Text Markdown Preview](https://github.com/revolunet/sublimetext-markdown-preview)

### PHP
* [Sublime Text Xdebug](https://github.com/martomo/SublimeTextXdebug)
* [Drupal](https://github.com/robballou/drupal-sublimetext)


### Useful helpers
* [AlignTab](https://github.com/randy3k/AlignTab)
* [HTML-CSS-JS Prettify](https://github.com/victorporof/Sublime-HTMLPrettify)
* [Sidebar Enhancements](https://github.com/titoBouzout/SideBarEnhancements)
* [Trailing Spaces](https://github.com/SublimeText/TrailingSpaces)

## Tips & Tricks

### SASS variable selection
Selecting a variable that is separated by dashes only selects part of that string by default. To fix this behaviour you can create an SCSS.sublime-settings file in */Users/{user}/Library/Application Support/Sublime Text 3/Packages/User* and add the following setting for word_separators:

```
"word_separators": "./\\()\"':,.;<>~!@#%^&*|+=[]{}`~?"
```

With this setting enabled, *$* and *-* are no longen seen as word-separators, causing you to select them as a single string.


### Clipboard History
In Sublime Text 3 you can activate the built-in clipboard history using *alt* + *cmd* + *V*.

	
#Services

##Getting started

Fire the app on a unix based system  

    npm start

Fire the app on a windows system.  

    npm run startwin

Install all dependencies (NPM and Bower), this happens automatically on `npm start`.

    npm install
    bower install

Running the tests pre-commit (I encourage you to install the pre-commit hook). 
To do this, execute the following code in the root of the API.

    ln -s ../../pre-commit.sh .git/hooks/pre-commit

##Importing Fixtures

Run `npm run fixtures` to install some users, projects, 
organisations and workitems in your database.  
**update** the existing features are temporary since rewrite. We are waiting for 
[TG-65](https://tree.taiga.io/project/moonio-blimp/task/65) to add new seeds...

##Migrating the Database

Migrations are new since the rewrite, they will update your database 
to the needed state. Add and remove fields, add and remove tables or even relations.
Hold on just a little more, we'll have em up and running soon.  
Until then you can just remove your .sqlite database 
and it will recreate it when you re-run the API.

##Usage

**Check out the [Wiki](/moonio/blimp-services/wiki) for all available endpoints and the parameters they require.**

##Running tests

To run all unit tests:

    npm test

optionally, code coverage can be generated during unit tests:

```bash
npm test --coverage
#or
npm run cover
```

##Got Issues?

Missing something? Got a cool idea? Request access to [Taiga.io](https://tree.taiga.io/project/moonio-blimp) 
planning board... Sander can invite you just like that, 
all you have to do is ask (even if you just want to watch (/creepy)).